# Api

```swift
    struct DeviceManager {
        getDevices() -> Observable<Device>

        getConnectedDevices() -> Observable<[Device]>

        getDevice() -> Observable<Device>
    }

    protocol Device {
        let giroscope: Giroscope?
        let isConnected: Boolean

        connect() -> Observable<Void>

        setSettings() -> Observable<>
    }

    struct MetaWearDevice: Device {}

    protocol DeviceSensor {
        listen(): Observable<T>
    }

    struct Giroscope: DeviceSensor {
        listen(): Observable<GiroscopeData>
    }

    struct GiroscopeData {}
```
