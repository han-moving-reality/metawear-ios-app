//
//  AccelerometerSensor.swift
//  WalkWright
//
//  Created by Stein Carsten Sinke on 21/10/2019.
//  Copyright © 2019 steincarstensinke. All rights reserved.
//

import Foundation
import MetaWear
import MetaWearCpp
import RxSwift

class AccelerometerSensor: DeviceSensor {
    
    private let device: MetaWear
    private var subject: PublishSubject<CartesianFloat>?
    private var dataSignal: OpaquePointer?
    
    init(_ device: MetaWear) {
        self.device = device
    }
    
    func listen() -> Observable<CartesianFloat>? {
        let board = device.board
        
        if !self.isFusionSensorAvailable(board: board) {
            return nil
        }
        
        getData(board)
        
        return subject
    }
    
    func stopListen() {
        self.subject?.dispose()
        guard let signal = dataSignal else {return}
        mbl_mw_datasignal_unsubscribe(signal)
    }
    
    private func getData(_ board: OpaquePointer?) {
        let linearAcc = mbl_mw_sensor_fusion_get_data_signal(board, MBL_MW_SENSOR_FUSION_DATA_CORRECTED_ACC)
        guard let la = linearAcc else { return }
        subject = PublishSubject<CartesianFloat>()
        
        dataSignal = la
        
        mbl_mw_datasignal_subscribe(la, bridge(obj: self)) { (context, data) in
            guard let context = context else { return }
            let accSensor: AccelerometerSensor = bridge(ptr: context)
                        
            // swiftlint:disable:next force_unwrapping
            let dataSignal: MblMwCorrectedCartesianFloat = data!.pointee.valueAs()
            let cartesianFloat = CartesianFloat(mblMwCartesianFloat: dataSignal)
            
            // swiftlint:disable:next force_unwrapping
            let sensor: AccelerometerSensor? = MetaWearDevice.getSensor(mac: accSensor.device.mac!, type: .accelerometer)
            sensor?.subject?.onNext(cartesianFloat)
        }
        
        mbl_mw_sensor_fusion_enable_data(board, MBL_MW_SENSOR_FUSION_DATA_CORRECTED_ACC)
        mbl_mw_sensor_fusion_start(board)
    }
    
}
