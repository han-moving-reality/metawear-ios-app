//
//  ConnectViewRow.swift
//  WalkWright
//
//  Created by Stein Carsten Sinke on 24/10/2019.
//  Copyright © 2019 steincarstensinke. All rights reserved.
//

import SwiftUI

struct ConnectViewRow: View {
    let device: Device
    
    var body: some View {
         HStack {
           VStack(alignment: .leading) {
            Text(device.name ?? device.id)
                   .font(.headline)
            Text(device.isConnected ? "Connected" : "Not Connected")
                   .font(.subheadline)
                .foregroundColor(.secondary)
           }
           Spacer()
           Circle()
            .fill(device.isConnected ? Color.green : Color.red)
               .frame(width: 10, height: 10)
       }
    }
}

//struct ConnectViewRow_Previews: PreviewProvider {
//    static var previews: some View {
//        ConnectViewRow(device: 1)
//    }
//}
