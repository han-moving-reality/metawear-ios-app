//
//  ConnectView.swift
//  WalkWright
//
//  Created by Stein Carsten Sinke on 24/10/2019.
//  Copyright © 2019 steincarstensinke. All rights reserved.
//

import SwiftUI
import RxSwift

struct ConnectView: View {
    @EnvironmentObject var deviceData: DeviceData
    
    var body: some View {
        NavigationView {
            List {
                ForEach(deviceData.devices, id: \.id) { device in
                    NavigationLink(destination: ConnectDetailView(device: device)) {
                        ConnectViewRow(device: device)
                    }
                }.onDelete(perform: performDelete)
            }
        .navigationBarTitle("Devices")
        }
    }
    
    func performDelete(at offsets: IndexSet) {
        for index in offsets {
            _ = deviceData.devices[index].disConnect()
        }
    }
}

struct ConnectView_Previews: PreviewProvider {
    static var previews: some View {
        ConnectView()
    }
}
