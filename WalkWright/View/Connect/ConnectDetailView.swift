//
//  ConnectDetailView.swift
//  WalkWright
//
//  Created by Stein Carsten Sinke on 24/10/2019.
//  Copyright © 2019 steincarstensinke. All rights reserved.
//

import SwiftUI
import Combine

struct ConnectDetailView: View {
    var device: Device
    @State private var name: String = "foobar"
    @State private var side: SideType = .left
    @State private var test = 0
    
    var body: some View {
        Form {
            Section {
                TextField("Name", text: $name)
                Button(action: setName) {
                    Text("UpdateName")
                }
            }
            
            Section {
                Picker(selection: $side, label: Text("Side:")) {
                    Text("Left").tag(SideType.left)
                    Text("Right").tag(SideType.right)
                }
                .pickerStyle(SegmentedPickerStyle())
                Button("Save") {
                    self.device.setSide(side: self.side)
                }
            }
            
            Section {
                Button(action: device.blinkLED) {
                    Text("Identify device")
                }.disabled(!device.isConnected)
            }
        }
        .navigationBarTitle(Text("Details"), displayMode: .inline)
        .navigationBarItems(trailing: Button(action: connect) {
            Text(device.isConnected ? "Disconnect" : "Connect")
        })
        .onAppear {
//            Assigning here because cannot assign in contructor
            self.name = self.device.name ?? self.device.id
            self.side = self.device.side
        }
    }
    
    func setName() {
        device.setName(name: name)
    }
    
    func connect() {
        if device.isConnected {
            _ = self.device.disConnect()
        } else {
            _ = self.device.connect()
        }
    }
}
//
//struct ConnectDetailView_Previews: PreviewProvider {
//    static var previews: some View {
////        NavigationView{
////            ConnectDetailView(device: Device())
//        }
//    }
//}
