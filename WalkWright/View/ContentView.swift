//
//  ContentView.swift
//  WalkWright
//
//  Created by Stein Carsten Sinke on 21/10/2019.
//  Copyright © 2019 Sinke & van der Vlist. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    @EnvironmentObject var deviceData: DeviceData
    
    var body: some View {
        TabView {
            RecordingView()
                .tabItem {
                    Image(systemName: "list.dash")
                    Text("Recording")
                }
            ConnectView()
                .tabItem {
                    Image(systemName: "ellipsis")
                    Text("Connect")
                }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView().environmentObject(DeviceData())
    }
}
