//
//  RecordingView.swift
//  WalkWright
//
//  Created by Stein Carsten Sinke on 24/10/2019.
//  Copyright © 2019 steincarstensinke. All rights reserved.
//

import SwiftUI
import RxSwift

struct RecordingView: View {
    private let disposeBag = DisposeBag()
    
    @State private var isRecording = false
    
    @State private var cartesianFloatL: CartesianFloat?
    @State private var cartesianFloatR: CartesianFloat?
    @State private var eulerAngleL: EulerAngle?
    @State private var eulerAngleR: EulerAngle?
    
    @EnvironmentObject var deviceData: DeviceData
    
    var body: some View {
        NavigationView {
            VStack {
                Button(action: {
                    self.toggleRecording()
                }, label: {
                    HStack {
                        Spacer()
                        
                        Image(systemName: isRecording ? "pause.fill" : "play.fill")
                        Text(isRecording ? "Stop" : "Record")
                            .font(.headline)
                        
                        Spacer()
                    }
                })
                .padding()
                .background(Color.accentColor)
                .foregroundColor(.white)
                .cornerRadius(7)
                
                List {
                    RecordingCartesionFloatView(side: "Left", cartesianFloat: cartesianFloatL)
                    RecordingEulerView(side: "Left", eulerAngle: eulerAngleL)
                    RecordingCartesionFloatView(side: "Right", cartesianFloat: cartesianFloatR)
                    RecordingEulerView(side: "Right", eulerAngle: eulerAngleR)
                }
            }
            .navigationBarTitle("Recording")
        }
    }
    
    func toggleRecording() {
        if isRecording {
            stopRecording()
        } else {
            startRecording()
        }
        
        isRecording = !isRecording
    }

    func startRecording() {
        for device in deviceData.devices {
            if !device.isConnected || device.side == .none { return }
            readAcc(device: device)
            readEuler(device: device)
        }
    }
    
    func stopRecording() {
        for device in deviceData.devices {
            if !device.isConnected || device.side == .none { return }
            stopAcc(device: device)
            stopEuler(device: device)
        }
    }
    
    func stopAcc(device: Device) {
        let sensor: AccelerometerSensor? = device.getSensor(type: .accelerometer)
        sensor?.stopListen()
    }
    
    func stopEuler(device: Device) {
        let sensor: EulerSensor? = device.getSensor(type: .euler)
        sensor?.stopListen()
    }

    func readAcc(device: Device) {
        let sensor: AccelerometerSensor? = device.getSensor(type: .accelerometer)
        
        DispatchQueue.global(qos: .background).async {
            sensor?.listen()?.subscribe { accEvent in
                guard let cartesianFloat = accEvent.element else { return }

                DispatchQueue.main.async {
                    if device.side == .left {
                        self.cartesianFloatL = cartesianFloat
                    } else if device.side == .right {
                        self.cartesianFloatR = cartesianFloat
                    }
                }

            }.disposed(by: self.disposeBag)
        }
    }

    func readEuler(device: Device) {
        let sensor: EulerSensor? = device.getSensor(type: .euler)

        DispatchQueue.global(qos: .background).async {
            sensor?.listen()?.subscribe { eulEvent in
                guard let eulerAngle = eulEvent.element else { return }

                DispatchQueue.main.async {
                    if device.side == .left {
                        self.eulerAngleL = eulerAngle
                    } else if device.side == .right {
                        self.eulerAngleR = eulerAngle
                    }
                }

            }.disposed(by: self.disposeBag)
        }
    }
}

struct RecordingView_Previews: PreviewProvider {
    static var previews: some View {
        RecordingView()
    }
}
