//
//  RecordingValueRow.swift
//  WalkWright
//
//  Created by Stein Carsten Sinke on 30/10/2019.
//  Copyright © 2019 steincarstensinke. All rights reserved.
//

import SwiftUI

struct RecordingValueRow: View {
    var label: String
    var value: Double?
    
    var body: some View {
        Text("\(label): \(value ?? 0.0, specifier: "%.2f")")
    }
}
