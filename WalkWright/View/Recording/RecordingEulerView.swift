//
//  RecordingEulerView.swift
//  WalkWright
//
//  Created by Stein Carsten Sinke on 30/10/2019.
//  Copyright © 2019 steincarstensinke. All rights reserved.
//

import SwiftUI

struct RecordingEulerView: View {
    var side: String
    var eulerAngle: EulerAngle?
    
    var body: some View {
        Section(header: Text("\(side) Euler")) {
            RecordingValueRow(label: "heading", value: eulerAngle?.heading)
            RecordingValueRow(label: "pitch", value: eulerAngle?.pitch)
            RecordingValueRow(label: "roll", value: eulerAngle?.roll)
            RecordingValueRow(label: "yaw", value: eulerAngle?.yaw)
        }
    }
}
