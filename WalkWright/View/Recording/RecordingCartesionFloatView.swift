//
//  RecordingCartesionFloatView.swift
//  WalkWright
//
//  Created by Stein Carsten Sinke on 30/10/2019.
//  Copyright © 2019 steincarstensinke. All rights reserved.
//

import SwiftUI

struct RecordingCartesionFloatView: View {
    var side: String
    var cartesianFloat: CartesianFloat?
    
    var body: some View {
        Section(header: Text("\(side) Cartesian float")) {
            RecordingValueRow(label: "x", value: cartesianFloat?.x)
            RecordingValueRow(label: "y", value: cartesianFloat?.y)
            RecordingValueRow(label: "z", value: cartesianFloat?.z)
        }
    }
}
