//
//  EulerAngle.swift
//  WalkWright
//
//  Created by Stein Carsten Sinke on 30/10/2019.
//  Copyright © 2019 steincarstensinke. All rights reserved.
//

import Foundation
import MetaWear
import MetaWearCpp

struct EulerAngle: Codable, SensorValue {
    typealias OutputType = EulerAngle
    
    let heading: Double
    let pitch: Double
    let roll: Double
    let yaw: Double
    
    init(mblMwEulerAngles: MblMwEulerAngles) {
        self.heading = Double(mblMwEulerAngles.heading)
        self.pitch = Double(mblMwEulerAngles.pitch)
        self.roll = Double(mblMwEulerAngles.roll)
        self.yaw = Double(mblMwEulerAngles.yaw)
        
    }
}
