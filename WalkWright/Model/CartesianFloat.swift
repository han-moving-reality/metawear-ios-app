//
//  CartesianFloat.swift
//  WalkWright
//
//  Created by Stein Carsten Sinke on 21/10/2019.
//  Copyright © 2019 steincarstensinke. All rights reserved.
//

import Foundation
import MetaWear
import MetaWearCpp

struct CartesianFloat: Codable, SensorValue {
    typealias OutputType = CartesianFloat

    // swiftlint:disable identifier_name
    let x: Double
    let y: Double
    let z: Double
    // swiftlint:enable identifier_name
    
    init(mblMwCartesianFloat: MblMwCorrectedCartesianFloat) {
        self.x = Double(mblMwCartesianFloat.x)
        self.y = Double(mblMwCartesianFloat.y)
        self.z = Double(mblMwCartesianFloat.z)
    }
}
