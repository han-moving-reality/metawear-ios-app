//
//  StringExtension.swift
//  WalkWright
//
//  Created by Stein Carsten Sinke on 28/10/2019.
//  Copyright © 2019 steincarstensinke. All rights reserved.
//

import Foundation

extension String {

  func toPointer() -> UnsafePointer<UInt8>? {
    guard let data = self.data(using: String.Encoding.utf8) else { return nil }

    let buffer = UnsafeMutablePointer<UInt8>.allocate(capacity: data.count)
    let stream = OutputStream(toBuffer: buffer, capacity: data.count)

    stream.open()
    data.withUnsafeBytes { (pointer: UnsafePointer<UInt8>) -> Void in
      stream.write(pointer, maxLength: data.count)
    }

    stream.close()

    return UnsafePointer<UInt8>(buffer)
  }
}
