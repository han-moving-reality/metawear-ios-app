//
//  Device.swift
//  WalkWright
//
//  Created by Stein Carsten Sinke on 21/10/2019.
//  Copyright © 2019 steincarstensinke. All rights reserved.
//

import Foundation
import RxSwift

protocol Device {
    var id: String { get }
    var name: String? { get }
    var isConnected: Bool { get }
    var side: SideType { get }
    
    func connect() -> Observable<Void>
    func disConnect() -> Observable<Void>
    func setName(name: String)
    func blinkLED()
    func setSide(side: SideType)
    func getSensor<T: DeviceSensor>(type: SensorType) -> T?
}
