//
//  SideType.swift
//  WalkWright
//
//  Created by Stein Carsten Sinke on 28/10/2019.
//  Copyright © 2019 steincarstensinke. All rights reserved.
//

import Foundation

enum SideType: String, CaseIterable, Hashable, Identifiable {
    case none
    case left
    case right
    var id: SideType {self}
}
