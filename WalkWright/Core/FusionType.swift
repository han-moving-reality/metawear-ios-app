//
//  FusionType.swift
//  WalkWright
//
//  Created by Stein Carsten Sinke on 30/10/2019.
//  Copyright © 2019 steincarstensinke. All rights reserved.
//

import Foundation
import MetaWear
import MetaWearCpp

enum FusionType {
    case m4g
    case ndof
}
