//
//  MetaWearDevice.swift
//  WalkWright
//
//  Created by Stein Carsten Sinke on 21/10/2019.
//  Copyright © 2019 steincarstensinke. All rights reserved.
//

import Combine
import CryptoKit
import Foundation
import MetaWear
import MetaWearCpp
import RxSwift

class MetaWearDevice: Device, ObservableObject {
    
    // C++ is a bitch, ARC removes a reference, so this is needed!!!
    
//    static var x : Dictionary<String , T>
    static var staticSensorDictionary: [String: [SensorType: AnyObject]] = [:]
    
    private let deviceRepository = DeviceRepository()
    private var accelerometerSensor: AccelerometerSensor
    private var eulerSensor: EulerSensor
    
    var device: MetaWear
    var side: SideType = .left
    
    var isConnected: Bool
    var name: String?
    
    private var _id: String = ""
    private(set) var id: String {
        get { _id }
        set {
            _id = newValue
        }
    }
    
    func setName(name: String) {
        let pointer = name.toPointer()
        mbl_mw_settings_set_device_name(device.board, pointer, UInt8(name.count))
        self.name = name
    }
    
    func setSide(side: SideType) {
        self.side = side
        deviceRepository.setDeviceData(mac: self.id, side: side)
    }
    
    init(device: MetaWear, mac: String, side: SideType, fusionType: FusionType) {
        self.device = device
        self._id = mac
        self.side = side
        self.isConnected = device.isConnectedAndSetup
        self.name = device.advertisementData["kCBAdvDataLocalName"] as? String
        self.accelerometerSensor = AccelerometerSensor(device)
        self.eulerSensor = EulerSensor(device)
        
        storeSensor(.accelerometer, self.accelerometerSensor)
        storeSensor(.euler, self.eulerSensor)
        
        setupBoard(device.board, fusionType)
    }
    
    private func storeSensor<T: DeviceSensor>(_ sensorType: SensorType, _ sensor: T) {
        let dictionary = MetaWearDevice.staticSensorDictionary.first { dic in
            dic.key == self.id
        }?.value

        if var dic = dictionary {
            dic.updateValue(sensor as AnyObject, forKey: sensorType)
            MetaWearDevice.staticSensorDictionary.updateValue(dic, forKey: self.id)
        } else {
            MetaWearDevice.staticSensorDictionary.updateValue([sensorType: sensor as AnyObject], forKey: self.id)
        }
    }
    
    private func setupBoard(_ board: OpaquePointer?, _ fusionType: FusionType) {
        switch fusionType {
        case .m4g:
            mbl_mw_sensor_fusion_set_mode(board, MBL_MW_SENSOR_FUSION_MODE_M4G)
        case .ndof:
            mbl_mw_sensor_fusion_set_mode(board, MBL_MW_SENSOR_FUSION_MODE_NDOF)
        }
        mbl_mw_sensor_fusion_set_acc_range(board, MBL_MW_SENSOR_FUSION_ACC_RANGE_8G)
        mbl_mw_sensor_fusion_write_config(board)
    }
    
    func getSensor<T: DeviceSensor>(type: SensorType) -> T? {
        let sensorsOpt = MetaWearDevice.staticSensorDictionary.first { $0.key == self._id }
        guard let sensors = sensorsOpt else { return nil }

        let sensor = sensors.value.first { $0.key == type }
        return sensor?.value as? T
    }
    
    static func getSensor<T: DeviceSensor>(mac: String, type: SensorType) -> T? {
        let sensorsOpt = MetaWearDevice.staticSensorDictionary.first { $0.key == mac }
        guard let sensors = sensorsOpt else { return nil }

        let sensor = sensors.value.first { $0.key == type }
        return sensor?.value as? T
    }

    func connect() -> Observable<Void> {
        let subject = PublishSubject<Void>()
        
        device.connectAndSetup().continueWith { task in
            if let error = task.error {
                subject.onError(error)
            } else {
                self.device.remember()
                self.isConnected = self.device.isConnectedAndSetup
                subject.onCompleted()
            }
        }
        
        return subject
    }
    
    func disConnect() -> Observable<Void> {
        let subject = PublishSubject<Void>()
        
        device.forget()
        device.cancelConnection()
        
        subject.onCompleted()
        self.objectWillChange.send()
        self.isConnected = device.isConnectedAndSetup
        return subject
    }
    
    func blinkLED() {
        var pattern = MblMwLedPattern()
        
        mbl_mw_led_load_preset_pattern(&pattern, MBL_MW_LED_PRESET_PULSE)
        mbl_mw_led_stop_and_clear(device.board)
        mbl_mw_led_write_pattern(device.board, &pattern, MBL_MW_LED_COLOR_GREEN)
        mbl_mw_led_play(device.board)
        
        DispatchQueue.global(qos: .background).async {
            sleep(5)
            mbl_mw_led_stop(self.device.board)

        }
    }
}
