//
//  DeviceManager.swift
//  WalkWright
//
//  Created by Stein Carsten Sinke on 21/10/2019.
//  Copyright © 2019 steincarstensinke. All rights reserved.
//

import Foundation
import RxSwift
import MetaWear
import MetaWearCpp

class DeviceManager {
    private let deviceRepository = DeviceRepository()
    
    func getDevices() -> PublishSubject<Device> {
        let subject = PublishSubject<Device>()
        MetaWearScanner.shared.startScan(allowDuplicates: false) { device in
            device.connectAndSetup().continueWith { _ in

                // swiftlint:disable:next force_unwrapping
                let side = self.deviceRepository.getDeviceSide(mac: device.mac!)
                subject.onNext(MetaWearDevice(device: device, mac: device.mac!, side: side, fusionType: .ndof))
                // swiftlint:disable:previous force_unwrapping

                MetaWearScanner.shared.retrieveSavedMetaWearsAsync().continueWith { task in
                    let rememberedDevices = task.result ?? []
                    
                    if !(rememberedDevices.contains(device)) {
                        device.cancelConnection()
                    }
                }
            }
        }
        
        return subject
    }
}
