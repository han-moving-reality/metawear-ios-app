//
//  SensorType.swift
//  WalkWright
//
//  Created by Stein Carsten Sinke on 29/10/2019.
//  Copyright © 2019 steincarstensinke. All rights reserved.
//

import Foundation

enum SensorType {
    case accelerometer
    case euler
}
