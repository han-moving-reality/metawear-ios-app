//
//  AccelerometerSensor.swift
//  WalkWright
//
//  Created by Stein Carsten Sinke on 21/10/2019.
//  Copyright © 2019 steincarstensinke. All rights reserved.
//

import Foundation
import MetaWear
import MetaWearCpp
import RxSwift

class EulerSensor: DeviceSensor {
    
    private let device: MetaWear
    private var subject: PublishSubject<EulerAngle>?
    private var dataSignal: OpaquePointer?
    
    init(_ device: MetaWear) {
        self.device = device
    }
    
    func listen() -> Observable<EulerAngle>? {
        let board = device.board

        if !self.isFusionSensorAvailable(board: board) {
            return nil
        }
        
        getData(board)
        
        return subject
    }
    
    func stopListen() {
        self.subject?.dispose()
        guard let signal = dataSignal else {return}
        mbl_mw_datasignal_unsubscribe(signal)
    }
    
    private func getData(_ board: OpaquePointer?) {
        let eulerAngle = mbl_mw_sensor_fusion_get_data_signal(board, MBL_MW_SENSOR_FUSION_DATA_EULER_ANGLE)
        guard let ea = eulerAngle else { return }
        subject = PublishSubject<EulerAngle>()
        
        dataSignal = ea
        
        mbl_mw_datasignal_subscribe(ea, bridge(obj: self)) { (context, data) in
            guard let context = context else { return }
            let eulerSensor: EulerSensor = bridge(ptr: context)
            
            // swiftlint:disable:next force_unwrapping
            let dataSignal: MblMwEulerAngles = data!.pointee.valueAs()
            let eulerAngle = EulerAngle(mblMwEulerAngles: dataSignal)
            
            // swiftlint:disable:next force_unwrapping
            let sensor: EulerSensor? = MetaWearDevice.getSensor(mac: eulerSensor.device.mac!, type: .euler)
            sensor?.subject?.onNext(eulerAngle)
        }
        
        mbl_mw_sensor_fusion_enable_data(board, MBL_MW_SENSOR_FUSION_DATA_EULER_ANGLE)
        mbl_mw_sensor_fusion_start(board)
    }
    
}
