//
//  DeviceSensor.swift
//  WalkWright
//
//  Created by Stein Carsten Sinke on 21/10/2019.
//  Copyright © 2019 steincarstensinke. All rights reserved.
//

import Foundation
import MetaWear
import MetaWearCpp
import RxSwift

protocol DeviceSensor {
    associatedtype Data
    
    func listen() -> Observable<Data>?
}

extension DeviceSensor {
    func isFusionSensorAvailable(board: OpaquePointer?) -> Bool {
        return mbl_mw_metawearboard_lookup_module(board, MBL_MW_MODULE_SENSOR_FUSION) != MBL_MW_MODULE_TYPE_NA
    }
}
