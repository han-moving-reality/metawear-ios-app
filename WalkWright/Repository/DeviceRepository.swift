//
//  DeviceRepository.swift
//  WalkWright
//
//  Created by Stein Carsten Sinke on 28/10/2019.
//  Copyright © 2019 steincarstensinke. All rights reserved.
//

import Foundation
import CoreData

struct DeviceRepository {
    private let ENTITYNAME = "MetaWearDeviceEntity"
    private let KEYMAC = "mac"
    private let KEYSIDE = "side"
    
    func setDeviceData(mac: String, side: SideType) {
        let context = getContext()
        context.mergePolicy = NSMergeByPropertyObjectTrumpMergePolicy
        guard let entity = NSEntityDescription.entity(forEntityName: ENTITYNAME, in: context) else {return}
        let device = NSManagedObject(entity: entity, insertInto: context)
         
        device.setValue(mac, forKeyPath: KEYMAC)
        device.setValue(side.rawValue, forKey: KEYSIDE)
        
        do {
          try context.save()
        } catch let error as NSError {
          print("Could not save. \(error), \(error.userInfo)")
        }
    }
    
    func getDeviceSide(mac: String) -> SideType {
        let context = getContext()
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: ENTITYNAME)

        do {
            let entities = try context.fetch(fetchRequest).filter { device in
                guard let macValue = device.value(forKey: KEYMAC) else { return false}
                return macValue as? String == mac
            }
            
            guard let entity = entities.first else { return SideType.none }
            guard let sideValue = entity.value(forKeyPath: KEYSIDE) as? String else { return SideType.none }
            
            return SideType(rawValue: sideValue) ?? SideType.none
            
        } catch {
            return SideType.none
        }
    }
    
    private func getContext() -> NSManagedObjectContext {
        let container = NSPersistentContainer(name: "DeviceModel")
        container.loadPersistentStores { _, error in
            if let error = error {
                fatalError("Unable to load persistent stores: \(error)")
            }
        }
        
        return container.viewContext
    }
}
