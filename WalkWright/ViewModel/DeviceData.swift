//
//  ContentViewModel.swift
//  WalkWright
//
//  Created by Stein Carsten Sinke on 24/10/2019.
//  Copyright © 2019 steincarstensinke. All rights reserved.
//

import Foundation
import RxSwift
import Combine

final class DeviceData: ObservableObject {
    private let disposeBag = DisposeBag()
    private let deviceManager = DeviceManager()
    
    @Published var devices: [Device] = []
    
    init() {
        getDevices()
    }
    
    func getDevices() {
        deviceManager.getDevices().subscribe { devicesEvent in
            guard let device = devicesEvent.element else { return }
            
            DispatchQueue.main.async {
                self.devices.append(device)
                print(self.devices)
            }
        }.disposed(by: disposeBag)
    }
}
